import Image from 'next/image'
import styles from './page.module.css'
import contohPng from '../public/contoh.png'
import Link from 'next/link'

export default function Home() {


  return (
    <main className={styles.main}>

      <div className={styles.grid}>
        <Link
          href="/api/redirect"
          className={styles.card}
        >
          <h2>
            PENCET DISINI <span>-&gt;</span>
          </h2>
          <p>Jangan lupa buka network, bersihin, liat responsenya HTTP Code 302 (atau kadang 307)</p>
        </Link>



      </div>

      <Image
          className={styles.logo}
          src={contohPng}
          alt="Contoh"

        />
    </main>
  )
}
